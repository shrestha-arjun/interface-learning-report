﻿using StudentRepository;
using StudentRepository.Interface;
using StudentRepository.JSON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface_Learning_Report
{
    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                Console.Write("Please make a selection :\n");
                Console.WriteLine("1. JSON data.\n" +
                    "2. CSV data.\n" +
                    "3. Exit.\n");

                string str = Console.ReadLine();
                switch (str)
                {
                    case "1":
                        IStudentRepository studentRepoJSON = new JSONRepository();
                        studentRepoJSON.GetAllStudents();
                        break;
                    case "2":
                        IStudentRepository studentRepoCSV = new CSVRepository();
                        studentRepoCSV.GetAllStudents();
                        break;
                    case "3":
                        Console.WriteLine("Thank You!!!");
                        Environment.Exit(0);
                        break;
                }
                Console.Read();
            } while (Console.ReadLine() != 3.ToString());
        }
    }
}
