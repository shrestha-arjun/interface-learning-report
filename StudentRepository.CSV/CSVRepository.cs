﻿using StudentRepository.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace StudentRepository
{
    public class CSVRepository : IStudentRepository
    {
        string path;

        public CSVRepository()
        {
            var CSVfile = ConfigurationManager.AppSettings["CSVFilename"];
            path = AppDomain.CurrentDomain.BaseDirectory + CSVfile;
        }

        public IEnumerable<Student> students { get; set; }

        public IEnumerable<Student> GetAllStudents()
        {
            var lines = File.ReadAllText(path).Split('\n');
            var studentCSV = from line in lines
                             select line.Split(',').ToArray();

            foreach (var student in studentCSV)
            {
                Console.WriteLine("ID: " + student[0]);
                Console.WriteLine("Roll Number: " + student[1]);
                Console.WriteLine("First Name: " + student[2]);
                Console.WriteLine("Last Name: " + student[3]);
                Console.WriteLine("Faculty: " + student[4]);
                Console.WriteLine();
            }

            return students;
        }
    }
}
