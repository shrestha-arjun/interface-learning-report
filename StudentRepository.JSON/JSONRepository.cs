﻿using Newtonsoft.Json;
using StudentRepository.Interface;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentRepository.JSON
{
    public class JSONRepository : IStudentRepository
    {
        string path;
        public JSONRepository()
        {
            var JSONFile = ConfigurationManager.AppSettings["JSONFilename"];
            path = AppDomain.CurrentDomain.BaseDirectory + JSONFile;
        }

        public IEnumerable<Student> students { get; set; }

        public IEnumerable<Student> GetAllStudents()
        {
            using (StreamReader streamReader = new StreamReader(path))
            {
                var result = streamReader.ReadToEnd();
                IEnumerable<Student> students = JsonConvert.DeserializeObject<IEnumerable<Student>>(result);

                foreach (var student in students)
                {
                    Console.WriteLine("ID: " + student.Id);
                    Console.WriteLine("Roll Number: " + student.RollNumber);
                    Console.WriteLine("First Name: " + student.FirstName);
                    Console.WriteLine("Second Name: " + student.LastName);
                    Console.WriteLine("Faculty: " + student.Faculty);
                    Console.WriteLine();
                }
            }
            return students;
        }
    }
}
